package com.nirmal.jeffrey.popularmovies.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import com.nirmal.jeffrey.popularmovies.LiveDataTestUtil;
import com.nirmal.jeffrey.popularmovies.TestUtils;
import com.nirmal.jeffrey.popularmovies.models.Movie;
import java.util.List;
import org.junit.Rule;
import org.junit.Test;

public class MovieDaoTest extends MovieDatabaseTest {

  private static final int ID = 123;
  private static final int PAGE_SIZE = 20;
  private static final int INDEX_ZERO = 0;
  @Rule
  public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

  @Test
  public void insertReadFromList() throws Exception {
    Movie movie = new Movie(TestUtils.TEST_MOVIE_ONE);
    //Insert
    getMovieDao().insertMovie(movie);
    LiveDataTestUtil<PagedList<Movie>> liveDataTestUtil = new LiveDataTestUtil<>();
    //Read
    PagedList<Movie> insertedMoviesPagedList = liveDataTestUtil.
        getValue(new LivePagedListBuilder<>(getMovieDao().getMovieList(), PAGE_SIZE).build());
    List<Movie> insertedMovies = insertedMoviesPagedList.snapshot();
    assertEquals(movie.getId(), insertedMovies.get(INDEX_ZERO).getId());
  }

  @Test
  public void insertReadDeleteRead() throws Exception {
    Movie movie = new Movie(TestUtils.TEST_MOVIE_ONE);
    movie.setId(ID);
    //Insert
    getMovieDao().insertMovie(movie);
    LiveDataTestUtil<Movie> liveDataTestUtil = new LiveDataTestUtil<>();
    //Read
    Movie insertedMovie = liveDataTestUtil.getValue(getMovieDao().getMovie(ID));
    assertNotNull(insertedMovie);
    assertEquals(movie.getId(), insertedMovie.getId());
    //Delete
    getMovieDao().deleteMovie(ID);
    //Read
    insertedMovie = liveDataTestUtil.getValue(getMovieDao().getMovie(ID));
    assertNull(insertedMovie);
  }
}
