package com.nirmal.jeffrey.popularmovies.database;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import org.junit.After;
import org.junit.Before;

public class MovieDatabaseTest {

  private MovieDatabase movieDatabase;

  public MovieDao getMovieDao() {
    return movieDatabase.movieDao();
  }

  @Before
  public void init(){
    movieDatabase = Room.inMemoryDatabaseBuilder(
        ApplicationProvider.getApplicationContext(),
        MovieDatabase.class).build();
  }

  @After
  public void finish(){
    movieDatabase.close();
  }
}
