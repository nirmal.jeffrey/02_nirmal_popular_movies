package com.nirmal.jeffrey.popularmovies.userinterface.activities;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import android.content.Intent;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;
import com.nirmal.jeffrey.popularmovies.R;
import com.nirmal.jeffrey.popularmovies.TestUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class DetailActivityIntentTest {

  private static final String TITLE = "Godzilla: King of the Monsters";
  private static final String MOVIE_INTENT_BUNDLE_KEY = "movie_from_list";
  private static final String BACKDROP_CONTENT_DESCRIPTION = "movie poster";

  @Rule
  public ActivityTestRule<DetailActivity> detailActivityTestRule =
      new ActivityTestRule<>(DetailActivity.class, true, false);

  @Before
  public void setUp() {
    Intent intent = new Intent();
    intent.putExtra(MOVIE_INTENT_BUNDLE_KEY, TestUtils.TEST_MOVIE_ONE);
    detailActivityTestRule.launchActivity(intent);
  }

  @Test
  public void validateTitleTextView() {
    onView(ViewMatchers.withId(R.id.tv_movie_detail_title)).check(matches(withText(TITLE)));
  }

  @Test
  public void validateBackDropContentDescription() {
    onView(withId(R.id.iv_movie_detail_backdrop))
        .check(matches(withContentDescription(BACKDROP_CONTENT_DESCRIPTION)));
  }

  @Test
  public void validateSnackBarVisibility() {
    onView(withId(R.id.fab_movie_detail_favorite)).perform(click());
    onView(withId(com.google.android.material.R.id.snackbar_text))
        .check(matches(isDisplayed()));
  }
}
