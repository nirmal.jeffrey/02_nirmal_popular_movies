package com.nirmal.jeffrey.popularmovies.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.DiffUtil.ItemCallback;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.nirmal.jeffrey.popularmovies.R;
import com.nirmal.jeffrey.popularmovies.adapters.MovieListAdapter.MovieViewHolder;
import com.nirmal.jeffrey.popularmovies.models.Movie;
import com.squareup.picasso.Picasso;

public class MovieListAdapter extends PagedListAdapter<Movie, MovieViewHolder> {
  private static final String POSTER_BASE_URL = "http://image.tmdb.org/t/p/w185/";
  private OnMovieClickListener movieClickListener;
  private static DiffUtil.ItemCallback<Movie> DIFF_CALLBACK = new ItemCallback<Movie>() {
    @Override
    public boolean areItemsTheSame(@NonNull Movie oldItem, @NonNull Movie newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull Movie oldItem, @NonNull Movie newItem) {
      return oldItem.equals(newItem);
    }
  };

  public MovieListAdapter(OnMovieClickListener movieClickListener) {
    super(DIFF_CALLBACK);
    this.movieClickListener = movieClickListener;
  }

  @NonNull
  @Override
  public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.movie_list_item,parent,false);
    return new MovieViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
    Movie movie = getItem(position);

    if(movie != null){
      String imageUrl = buildMovieImageURLString(movie.getPosterPath());
      Picasso.get()
          .load(imageUrl)
          .into(holder.posterImageView);
    }
  }

  class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    @BindView(R.id.iv_movie_list_item_poster)
    ImageView posterImageView;

    MovieViewHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(this,itemView);
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
      int position = getAdapterPosition();

      if(movieClickListener != null && position != RecyclerView.NO_POSITION) {
        Movie movie = getItem(position);
        movieClickListener.onMovieClick(movie);
      }
    }
  }

  private static String buildMovieImageURLString(String imagePath) {
    return POSTER_BASE_URL + imagePath;
  }

  public interface OnMovieClickListener{
    void onMovieClick(Movie movie);
  }
}
