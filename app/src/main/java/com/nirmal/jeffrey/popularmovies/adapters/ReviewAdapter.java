package com.nirmal.jeffrey.popularmovies.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import at.blogc.android.views.ExpandableTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.nirmal.jeffrey.popularmovies.R;
import com.nirmal.jeffrey.popularmovies.adapters.ReviewAdapter.ReviewViewHolder;
import com.nirmal.jeffrey.popularmovies.models.Review;
import java.util.ArrayList;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewViewHolder> {

  private ArrayList<Review> reviewArrayList;

  public void setReviewListData(ArrayList<Review> reviewArrayList) {
    this.reviewArrayList = reviewArrayList;
    notifyDataSetChanged();
  }

  @NonNull
  @Override
  public ReviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.review_list_item, parent, false);
    return new ReviewViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ReviewViewHolder holder, int position) {
    Review review = reviewArrayList.get(position);
    holder.reviewerTextView.setText(String.format(" - %s", review.getAuthor()));
    holder.expandableTextView.setText(review.getContent());
    holder.expandableTextView.setInterpolator(new OvershootInterpolator());
    holder.reviewLayout.setOnClickListener(view -> {
      holder.expandCollapseView.setImageResource(
          holder.expandableTextView.isExpanded() ? R.drawable.ic_expand_more_24dp
              : R.drawable.ic_expand_less_24dp);
      holder.expandableTextView.toggle();
    });
  }

  @Override
  public int getItemCount() {
    if (reviewArrayList == null) {
      return 0;
    }
    return reviewArrayList.size();
  }

  class ReviewViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.reviewer_name)
    TextView reviewerTextView;
    @BindView(R.id.review_content)
    ExpandableTextView expandableTextView;
    @BindView(R.id.review_expand_collapse_button)
    ImageView expandCollapseView;
    @BindView(R.id.review_constraint_layout)
    ConstraintLayout reviewLayout;

    ReviewViewHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
