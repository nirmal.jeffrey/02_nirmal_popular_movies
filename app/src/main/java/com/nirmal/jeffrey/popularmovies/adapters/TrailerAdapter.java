package com.nirmal.jeffrey.popularmovies.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.nirmal.jeffrey.popularmovies.R;
import com.nirmal.jeffrey.popularmovies.adapters.TrailerAdapter.TrailerViewHolder;
import com.nirmal.jeffrey.popularmovies.models.Trailer;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class TrailerAdapter extends RecyclerView.Adapter<TrailerViewHolder> {

  private ArrayList<Trailer> trailers;
  private OnTrailerClickListener trailerClickListener;
  private static final String MOVIE_TRAILER_THUMBNAIL_URL_PART_ONE = "https://img.youtube.com/vi/";
  private static final String MOVIE_TRAILER_THUMBNAIL_URL_PART_TWO = "/0.jpg";

  public TrailerAdapter(OnTrailerClickListener trailerClickListener) {
    this.trailerClickListener = trailerClickListener;
  }

  public void setTrailerListData(ArrayList<Trailer> trailerList) {
    this.trailers = trailerList;
    notifyDataSetChanged();
  }

  @NonNull
  @Override
  public TrailerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.trailer_list_item, parent, false);

    return new TrailerViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull TrailerViewHolder holder, int position) {
    Trailer trailer = trailers.get(position);
    if (trailer.getKey() != null) {
      String thumbnailUrl = buildTrailerThumbnailUrl(trailer.getKey());
      Picasso.get().load(thumbnailUrl).into(holder.trailerImageView);
    }
  }

  @Override
  public int getItemCount() {
    if (trailers == null) {
      return 0;
    }

    return trailers.size();
  }

  class TrailerViewHolder extends RecyclerView.ViewHolder implements OnClickListener {

    @BindView(R.id.trailer_image_view)
    ImageView trailerImageView;

    TrailerViewHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
      int position = getAdapterPosition();

      if (trailerClickListener != null && position != RecyclerView.NO_POSITION) {
        trailerClickListener.onTrailerClick(trailers.get(position));
      }
    }
  }

  private static String buildTrailerThumbnailUrl(String key) {
    return MOVIE_TRAILER_THUMBNAIL_URL_PART_ONE + key + MOVIE_TRAILER_THUMBNAIL_URL_PART_TWO;
  }

  public interface OnTrailerClickListener {

    void onTrailerClick(Trailer trailer);
  }
}
