package com.nirmal.jeffrey.popularmovies.dagger.component;

import android.app.Application;
import com.nirmal.jeffrey.popularmovies.BaseApplication;
import com.nirmal.jeffrey.popularmovies.dagger.modules.ActivityModule;
import com.nirmal.jeffrey.popularmovies.dagger.modules.AppModule;
import com.nirmal.jeffrey.popularmovies.dagger.modules.FragmentModule;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import javax.inject.Singleton;

@Singleton
@Component(
    modules = {
        AndroidSupportInjectionModule.class,
        ActivityModule.class,
        FragmentModule.class,
        AppModule.class})
public interface AppComponent extends AndroidInjector<BaseApplication> {

  @Component.Builder
  interface Builder {

    @BindsInstance
    Builder application(Application application);

    AppComponent build();
  }
}
