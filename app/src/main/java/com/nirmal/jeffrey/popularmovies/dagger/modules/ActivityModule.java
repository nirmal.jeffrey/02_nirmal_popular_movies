package com.nirmal.jeffrey.popularmovies.dagger.modules;

import com.nirmal.jeffrey.popularmovies.userinterface.activities.DetailActivity;
import com.nirmal.jeffrey.popularmovies.userinterface.activities.MainActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

  @ContributesAndroidInjector
  abstract MainActivity contributeMainActivity();

  @ContributesAndroidInjector(modules = FragmentModule.class)
  abstract DetailActivity contributeDetailActivity();
}
