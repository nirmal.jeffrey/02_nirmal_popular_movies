package com.nirmal.jeffrey.popularmovies.dagger.modules;

import android.app.Application;
import androidx.room.Room;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nirmal.jeffrey.popularmovies.BuildConfig;
import com.nirmal.jeffrey.popularmovies.database.MovieDao;
import com.nirmal.jeffrey.popularmovies.database.MovieDatabase;
import com.nirmal.jeffrey.popularmovies.network.MovieApi;
import com.nirmal.jeffrey.popularmovies.repository.MovieRepository;
import dagger.Module;
import dagger.Provides;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javax.inject.Singleton;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class)
public class AppModule {

  private static final String DATABASE_NAME = "movie_database";
  private static final String API_KEY_PARAMETER = "api_key";
  private static final String API_KEY_VALUE = BuildConfig.ApiKey;
  private static final String BASE_URL = "https://api.themoviedb.org/3/";

  @Provides
  @Singleton
  MovieDao provideMovieDao(MovieDatabase database) {
    return database.movieDao();
  }

  @Provides
  @Singleton
  MovieDatabase provideMovieDatabase(Application application) {
    return Room.databaseBuilder(application, MovieDatabase.class, DATABASE_NAME).build();
  }

  @Provides
  Executor provideExecutor() {
    return Executors.newSingleThreadExecutor();
  }

  @Provides
  @Singleton
  MovieRepository provideMovieRepository(MovieDao movieDao, MovieApi movieApi, Executor executor) {
    return new MovieRepository(movieDao, movieApi, executor);
  }


  @Provides
  Gson provideGson() {
    return new GsonBuilder().create();
  }

  @Provides
  Retrofit provideRetrofit(Gson gson, OkHttpClient client) {
    return new Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build();
  }

  @Provides
  @Singleton
  MovieApi provideMovieApi(Retrofit retrofit) {
    return retrofit.create(MovieApi.class);
  }

  @Provides
  OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor) {
    return new Builder().addInterceptor(chain -> {
      Request originalRequest = chain.request();
      HttpUrl originalUrl = originalRequest.url();
      HttpUrl modifiedUrl = originalUrl.newBuilder()
          .addQueryParameter(API_KEY_PARAMETER, API_KEY_VALUE)
          .build();
      Request.Builder builder = originalRequest.newBuilder().url(modifiedUrl);
      return chain.proceed(builder.build());
    }).addInterceptor(interceptor)
        .build();
  }

  @Provides
  HttpLoggingInterceptor provideHttpLoggingInterceptor() {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.level(Level.BODY);
    return interceptor;
  }


}
