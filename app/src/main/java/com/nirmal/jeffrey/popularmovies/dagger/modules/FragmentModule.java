package com.nirmal.jeffrey.popularmovies.dagger.modules;

import com.nirmal.jeffrey.popularmovies.userinterface.fragment.MovieDetailFragment;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {

  @ContributesAndroidInjector
  abstract MovieDetailFragment contributeMovieDetailFragment();
}
