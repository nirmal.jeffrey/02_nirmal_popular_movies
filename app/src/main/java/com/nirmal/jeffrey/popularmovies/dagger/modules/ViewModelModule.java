package com.nirmal.jeffrey.popularmovies.dagger.modules;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.nirmal.jeffrey.popularmovies.dagger.key.ViewModelKey;
import com.nirmal.jeffrey.popularmovies.viewmodels.MovieDetailViewModel;
import com.nirmal.jeffrey.popularmovies.viewmodels.MovieListViewModel;
import com.nirmal.jeffrey.popularmovies.viewmodels.MovieViewModelFactory;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

  @Binds
  @IntoMap
  @ViewModelKey(MovieListViewModel.class)
  abstract ViewModel bindMovieListViewModel(MovieListViewModel movieListViewModel);

  @Binds
  abstract ViewModelProvider.Factory bindViewModelFactory(MovieViewModelFactory factory);

  @Binds
  @IntoMap
  @ViewModelKey(MovieDetailViewModel.class)
  abstract ViewModel bindMovieDetailViewModel(MovieDetailViewModel movieDetailViewModel);


}
