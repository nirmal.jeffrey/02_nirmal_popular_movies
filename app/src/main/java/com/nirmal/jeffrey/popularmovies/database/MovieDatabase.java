package com.nirmal.jeffrey.popularmovies.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import com.nirmal.jeffrey.popularmovies.models.Movie;

@Database(entities = {Movie.class}, version = 1, exportSchema = false)
public abstract class MovieDatabase extends RoomDatabase {

private static volatile MovieDatabase instance;

public abstract MovieDao movieDao();
}
