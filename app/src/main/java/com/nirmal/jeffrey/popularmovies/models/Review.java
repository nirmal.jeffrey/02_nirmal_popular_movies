package com.nirmal.jeffrey.popularmovies.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;

public class Review implements Parcelable {
  @SerializedName("author")
  @Expose
  private String author;
 @SerializedName("content")
 @Expose
 private String content;

  public Review(String author, String content) {
    this.author = author;
    this.content = content;
  }

  protected Review(Parcel in) {
    author = in.readString();
    content = in.readString();
  }

  public static final Creator<Review> CREATOR = new Creator<Review>() {
    @Override
    public Review createFromParcel(Parcel in) {
      return new Review(in);
    }

    @Override
    public Review[] newArray(int size) {
      return new Review[size];
    }
  };

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(author);
    parcel.writeString(content);
  }

  @NotNull
  @Override
  public String toString() {
    return "Review{" +
        "author='" + author + '\'' +
        ", content='" + content + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }

    if (!(obj instanceof Review)) {
      return false;
    }
    Review review = (Review) obj;

    return getAuthor().equals(review.getAuthor()) && getContent().equals(review.getContent());
  }
}
