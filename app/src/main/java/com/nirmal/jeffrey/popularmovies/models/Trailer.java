package com.nirmal.jeffrey.popularmovies.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;

public class Trailer implements Parcelable {
  @SerializedName("key")
  @Expose
  private  String key;
  @SerializedName("name")
  @Expose
  private String name;

  public Trailer(String key, String name) {
    this.key = key;
    this.name = name;
  }

  protected Trailer(Parcel in) {
    key = in.readString();
    name = in.readString();
  }

  public static final Creator<Trailer> CREATOR = new Creator<Trailer>() {
    @Override
    public Trailer createFromParcel(Parcel in) {
      return new Trailer(in);
    }

    @Override
    public Trailer[] newArray(int size) {
      return new Trailer[size];
    }
  };

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(key);
    parcel.writeString(name);
  }

  @NotNull
  @Override
  public String toString() {
    return "Trailer{" +
        "key='" + key + '\'' +
        ", name='" + name + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Trailer)) {
      return false;
    }
    Trailer trailer = (Trailer) o;
    return getKey().equals(trailer.getKey()) &&
        getName().equals(trailer.getName());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getKey(), getName());
  }
}
