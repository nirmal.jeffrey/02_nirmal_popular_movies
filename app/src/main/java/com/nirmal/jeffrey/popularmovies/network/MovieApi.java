package com.nirmal.jeffrey.popularmovies.network;

import com.nirmal.jeffrey.popularmovies.network.responses.MovieListResponse;
import com.nirmal.jeffrey.popularmovies.network.responses.ReviewListResponse;
import com.nirmal.jeffrey.popularmovies.network.responses.TrailerListResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieApi {

  @GET("movie/{movie_list_type}")
  Observable<MovieListResponse> getMovieListByType(
      @Path("movie_list_type") String movieListType,
      @Query("page") int page);

  @GET("movie/{movie_id}/videos")
  Observable<TrailerListResponse> getTrailerList(@Path("movie_id") int movieId);

  @GET("movie/{movie_id}/reviews")
  Observable<ReviewListResponse> getReviewList(@Path("movie_id") int movieId);


}
