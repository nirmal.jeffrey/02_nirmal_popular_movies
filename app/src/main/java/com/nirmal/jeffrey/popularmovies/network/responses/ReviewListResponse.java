package com.nirmal.jeffrey.popularmovies.network.responses;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nirmal.jeffrey.popularmovies.models.Review;
import java.util.List;

public class ReviewListResponse implements Parcelable {

  public static final Creator<ReviewListResponse> CREATOR = new Creator<ReviewListResponse>() {
    @Override
    public ReviewListResponse createFromParcel(Parcel in) {
      return new ReviewListResponse(in);
    }

    @Override
    public ReviewListResponse[] newArray(int size) {
      return new ReviewListResponse[size];
    }
  };
  @SerializedName("id")
  @Expose
  private int id;
  @SerializedName("results")
  @Expose
  private List<Review> results;


  public ReviewListResponse(int id,
      List<Review> results) {
    this.id = id;
    this.results = results;
  }

  protected ReviewListResponse(Parcel in) {
    id = in.readInt();
    results = in.createTypedArrayList(Review.CREATOR);
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public List<Review> getResults() {
    return results;
  }

  public void setResults(List<Review> results) {
    this.results = results;
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int i) {
    parcel.writeInt(id);
    parcel.writeTypedList(results);
  }
}
