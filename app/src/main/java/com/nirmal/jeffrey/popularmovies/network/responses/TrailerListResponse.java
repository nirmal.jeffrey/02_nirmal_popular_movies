package com.nirmal.jeffrey.popularmovies.network.responses;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nirmal.jeffrey.popularmovies.models.Trailer;
import java.util.List;

public class TrailerListResponse implements Parcelable {

  public static final Creator<TrailerListResponse> CREATOR = new Creator<TrailerListResponse>() {
    @Override
    public TrailerListResponse createFromParcel(Parcel in) {
      return new TrailerListResponse(in);
    }

    @Override
    public TrailerListResponse[] newArray(int size) {
      return new TrailerListResponse[size];
    }
  };

  @SerializedName("id")
  @Expose
  private int id;
  @SerializedName("results")
  @Expose
  private List<Trailer> results;


  public TrailerListResponse(int id,
      List<Trailer> results) {
    this.id = id;
    this.results = results;
  }

  protected TrailerListResponse(Parcel in) {
    id = in.readInt();
    results = in.createTypedArrayList(Trailer.CREATOR);
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public List<Trailer> getResults() {
    return results;
  }

  public void setResults(List<Trailer> results) {
    this.results = results;
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int i) {
    parcel.writeInt(id);
    parcel.writeTypedList(results);
  }
}
