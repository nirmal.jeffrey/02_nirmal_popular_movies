package com.nirmal.jeffrey.popularmovies.paging;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PageKeyedDataSource;
import com.nirmal.jeffrey.popularmovies.models.Movie;
import com.nirmal.jeffrey.popularmovies.network.MovieApi;
import com.nirmal.jeffrey.popularmovies.network.responses.MovieListResponse;
import com.nirmal.jeffrey.popularmovies.utilites.NetworkStatus;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PopularMoviesDataSource extends PageKeyedDataSource<Integer, Movie> {

  private static final String POPULAR_MOVIE_LIST_TYPE = "popular";
  private MovieApi movieApi;
  private static final int FIRST_PAGE = 1;
  private MutableLiveData<NetworkStatus> networkState = new MutableLiveData<>();

  public PopularMoviesDataSource(MovieApi movieApi) {
    this.movieApi = movieApi;

  }

  @Override
  public void loadInitial(@NonNull LoadInitialParams<Integer> params,
      @NonNull LoadInitialCallback<Integer, Movie> callback) {

    movieApi.getMovieListByType(POPULAR_MOVIE_LIST_TYPE, FIRST_PAGE).subscribeOn(Schedulers.io())
        .subscribe(new Observer<MovieListResponse>() {
          @Override
          public void onSubscribe(Disposable d) {
            networkState.postValue(NetworkStatus.LOADING);
          }

          @Override
          public void onNext(MovieListResponse movieListResponse) {

            if (!movieListResponse.getMovieList().isEmpty()) {
              networkState.postValue(NetworkStatus.SUCCESS);
              int nextPage = FIRST_PAGE + 1;
              callback.onResult(movieListResponse.getMovieList(), null, nextPage);
            } else {
              networkState.postValue(NetworkStatus.EMPTY);
            }
          }

          @Override
          public void onError(Throwable e) {
            networkState.postValue(NetworkStatus.ERROR);
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void loadBefore(@NonNull LoadParams<Integer> params,
      @NonNull LoadCallback<Integer, Movie> callback) {

    movieApi.getMovieListByType(POPULAR_MOVIE_LIST_TYPE, params.key).subscribeOn(Schedulers.io())
        .subscribe(new Observer<MovieListResponse>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(MovieListResponse movieListResponse) {

            if (!movieListResponse.getMovieList().isEmpty()) {
              networkState.postValue(NetworkStatus.SUCCESS);
              Integer key = (params.key > 1) ? params.key - 1 : null;
              callback.onResult(movieListResponse.getMovieList(), key);
            }
          }

          @Override
          public void onError(Throwable e) {
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void loadAfter(@NonNull LoadParams<Integer> params,
      @NonNull LoadCallback<Integer, Movie> callback) {

    movieApi.getMovieListByType(POPULAR_MOVIE_LIST_TYPE, params.key).subscribeOn(Schedulers.io())
        .subscribe(new Observer<MovieListResponse>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(MovieListResponse movieListResponse) {

            if (!movieListResponse.getMovieList().isEmpty()) {
              networkState.postValue(NetworkStatus.SUCCESS);
              int nextPageNumber = params.key + 1;
              int totalPages = movieListResponse.getTotalPages();
              Integer key = (totalPages > nextPageNumber) ? nextPageNumber : null;
              callback.onResult(movieListResponse.getMovieList(), key);
            }
          }

          @Override
          public void onError(Throwable e) {
          }

          @Override
          public void onComplete() {

          }
        });
  }

  public MutableLiveData<NetworkStatus> getNetworkState() {
    return networkState;
  }
}
