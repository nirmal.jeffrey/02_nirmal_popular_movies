package com.nirmal.jeffrey.popularmovies.paging;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import com.nirmal.jeffrey.popularmovies.network.MovieApi;

public class PopularMoviesDataSourceFactory extends DataSource.Factory {

  private MovieApi movieApi;
  private MutableLiveData<PopularMoviesDataSource> popularMoviesDataSource = new MutableLiveData<>();

  public PopularMoviesDataSourceFactory(MovieApi movieApi) {
    this.movieApi = movieApi;
  }

  @NonNull
  @Override
  public DataSource create() {
    PopularMoviesDataSource dataSource = new PopularMoviesDataSource(movieApi);
    popularMoviesDataSource.postValue(dataSource);
    return dataSource;
  }

  public MutableLiveData<PopularMoviesDataSource> getPopularMoviesDataSource() {
    return popularMoviesDataSource;
  }

}
