package com.nirmal.jeffrey.popularmovies.paging;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import com.nirmal.jeffrey.popularmovies.network.MovieApi;

public class TopRatedMoviesDataSourceFactory extends DataSource.Factory {

  private MovieApi movieApi;
  private MutableLiveData<TopRatedMoviesDataSource> topRatedMoviesDataSource = new MutableLiveData<>();

  public TopRatedMoviesDataSourceFactory(MovieApi movieApi) {
    this.movieApi = movieApi;
  }

  @NonNull
  @Override
  public DataSource create() {
    TopRatedMoviesDataSource dataSource = new TopRatedMoviesDataSource(movieApi);
    topRatedMoviesDataSource.postValue(dataSource);
    return dataSource;
  }

  public LiveData<TopRatedMoviesDataSource> getTopRatedMoviesDataSource() {
    return topRatedMoviesDataSource;
  }
}
