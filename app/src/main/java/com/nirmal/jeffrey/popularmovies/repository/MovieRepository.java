package com.nirmal.jeffrey.popularmovies.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import com.nirmal.jeffrey.popularmovies.database.MovieDao;
import com.nirmal.jeffrey.popularmovies.models.Movie;
import com.nirmal.jeffrey.popularmovies.models.Review;
import com.nirmal.jeffrey.popularmovies.models.Trailer;
import com.nirmal.jeffrey.popularmovies.network.MovieApi;
import com.nirmal.jeffrey.popularmovies.network.responses.ReviewListResponse;
import com.nirmal.jeffrey.popularmovies.network.responses.TrailerListResponse;
import com.nirmal.jeffrey.popularmovies.paging.PopularMoviesDataSource;
import com.nirmal.jeffrey.popularmovies.paging.PopularMoviesDataSourceFactory;
import com.nirmal.jeffrey.popularmovies.paging.TopRatedMoviesDataSource;
import com.nirmal.jeffrey.popularmovies.paging.TopRatedMoviesDataSourceFactory;
import com.nirmal.jeffrey.popularmovies.utilites.NetworkStatus;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import java.util.concurrent.Executor;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MovieRepository {

  private static PagedList.Config config = new PagedList.Config.Builder()
      .setPageSize(20)
      .setEnablePlaceholders(false)
      .build();
  private LiveData<NetworkStatus> popularMoviesStatus = new MutableLiveData<>();
  private LiveData<NetworkStatus> topRatedMoviesStatus = new MutableLiveData<>();
  private MutableLiveData<List<Trailer>> trailerLiveData = new MutableLiveData<>();
  private MutableLiveData<List<Review>> reviewLiveData = new MutableLiveData<>();
  private MovieApi movieApi;
  private MovieDao movieDao;
  private Executor executor;
  private PopularMoviesDataSourceFactory popularMoviesDataSourceFactory;
  private TopRatedMoviesDataSourceFactory topRatedMoviesDataSourceFactory;

  @Inject
  public MovieRepository(MovieDao movieDao, MovieApi movieApi, Executor executor) {
    this.movieDao = movieDao;
    this.movieApi = movieApi;
    this.executor = executor;
  }


  public LiveData<PagedList<Movie>> getPagedPopularMoviesLiveData() {
    popularMoviesDataSourceFactory = new PopularMoviesDataSourceFactory(movieApi);
    return new LivePagedListBuilder<Integer, Movie>(popularMoviesDataSourceFactory, config).build();
  }

  public LiveData<PagedList<Movie>> getPagedTopRatedMoviesLiveData() {
    topRatedMoviesDataSourceFactory = new TopRatedMoviesDataSourceFactory(movieApi);
    return new LivePagedListBuilder<Integer, Movie>(topRatedMoviesDataSourceFactory, config)
        .build();
  }

  public LiveData<PagedList<Movie>> getPagedFavoriteMoviesListData() {
    return new LivePagedListBuilder<>(movieDao.getMovieList(), config).build();
  }

  public void invalidatePopularDataSource() {

    if (popularMoviesDataSourceFactory != null) {
      PopularMoviesDataSource dataSource = popularMoviesDataSourceFactory
          .getPopularMoviesDataSource().getValue();

      if (dataSource != null) {
        dataSource.invalidate();
      }
    }
  }

  public void invalidateTopRatedDataSource() {
    TopRatedMoviesDataSource dataSource = topRatedMoviesDataSourceFactory
        .getTopRatedMoviesDataSource().getValue();

    if (dataSource != null) {
      dataSource.invalidate();
    }
  }

  public void initStatus() {
    popularMoviesStatus = Transformations
        .switchMap(popularMoviesDataSourceFactory.getPopularMoviesDataSource(),
            PopularMoviesDataSource::getNetworkState);
    topRatedMoviesStatus = Transformations
        .switchMap(topRatedMoviesDataSourceFactory.getTopRatedMoviesDataSource(),
            TopRatedMoviesDataSource::getNetworkState);
  }

  public LiveData<NetworkStatus> getPopularMoviesStatus() {
    return popularMoviesStatus;
  }

  public LiveData<NetworkStatus> getTopRatedMoviesStatus() {
    return topRatedMoviesStatus;
  }

  public void subscribeToTrailerList(int movieId) {
    movieApi.getTrailerList(movieId).subscribeOn(Schedulers.io())
        .subscribe(new Observer<TrailerListResponse>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(TrailerListResponse response) {
            trailerLiveData.postValue(response.getResults());
          }

          @Override
          public void onError(Throwable e) {
            trailerLiveData.postValue(null);
          }

          @Override
          public void onComplete() {

          }
        });
  }

  public void subscribeToReviewList(int movieId) {

    movieApi.getReviewList(movieId).subscribeOn(Schedulers.io())
        .subscribe(new Observer<ReviewListResponse>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(ReviewListResponse reviewListResponse) {
            reviewLiveData.postValue(reviewListResponse.getResults());
          }

          @Override
          public void onError(Throwable e) {
            reviewLiveData.postValue(null);
          }

          @Override
          public void onComplete() {

          }
        });
  }

  public MutableLiveData<List<Trailer>> getTrailerLiveData() {
    return trailerLiveData;
  }

  public MutableLiveData<List<Review>> getReviewLiveData() {
    return reviewLiveData;
  }

  public LiveData<Movie> getMovieFromDB(int movieId) {
    return movieDao.getMovie(movieId);
  }

  public void insertMovieIntoDB(Movie movie) {
    executor.execute(() -> movieDao.insertMovie(movie));
  }

  public void deleteMovieFromDB(int movieId) {
    executor.execute(() -> movieDao.deleteMovie(movieId));
  }
}
