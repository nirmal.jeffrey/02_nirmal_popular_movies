package com.nirmal.jeffrey.popularmovies.userinterface.activities;

import static com.nirmal.jeffrey.popularmovies.userinterface.activities.MainActivity.MOVIE_INTENT_BUNDLE_KEY;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.nirmal.jeffrey.popularmovies.R;
import com.nirmal.jeffrey.popularmovies.models.Movie;
import com.nirmal.jeffrey.popularmovies.userinterface.fragment.MovieDetailFragment;
import dagger.android.support.DaggerAppCompatActivity;

public class DetailActivity extends DaggerAppCompatActivity {

  private Movie movie;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail);
    Intent intent = getIntent();
    if (intent.getExtras() != null) {
      movie = intent.getExtras().getParcelable(MOVIE_INTENT_BUNDLE_KEY);
    }
    if (movie != null) {
      getSupportFragmentManager().beginTransaction()
          .replace(R.id.movie_detail_container, MovieDetailFragment.getInstance(movie))
          .commit();
    } else {
      closeOnError();
    }
  }

  private void closeOnError() {
    Toast.makeText(this, getString(R.string.action_close_detail_activity), Toast.LENGTH_SHORT)
        .show();
    finish();
  }
}
