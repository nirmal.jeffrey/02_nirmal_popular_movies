package com.nirmal.jeffrey.popularmovies.userinterface.activities;

import static com.nirmal.jeffrey.popularmovies.utilites.MoviesViewState.FAVORITES;
import static com.nirmal.jeffrey.popularmovies.utilites.MoviesViewState.POPULAR;
import static com.nirmal.jeffrey.popularmovies.utilites.MoviesViewState.TOP_RATED;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.nirmal.jeffrey.popularmovies.R;
import com.nirmal.jeffrey.popularmovies.adapters.MovieListAdapter;
import com.nirmal.jeffrey.popularmovies.adapters.MovieListAdapter.OnMovieClickListener;
import com.nirmal.jeffrey.popularmovies.models.Movie;
import com.nirmal.jeffrey.popularmovies.userinterface.fragment.MovieDetailFragment;
import com.nirmal.jeffrey.popularmovies.utilites.MoviesViewState;
import com.nirmal.jeffrey.popularmovies.viewmodels.MovieListViewModel;
import com.nirmal.jeffrey.popularmovies.viewmodels.MovieViewModelFactory;
import dagger.android.support.DaggerAppCompatActivity;
import javax.inject.Inject;


public class MainActivity extends DaggerAppCompatActivity implements OnMovieClickListener {

  private static final int SCREEN_WIDTH_700_DP = 700;
  private static final int INDEX_ZERO = 0;
  private static final int GRID_LAYOUT_SPAN_COUNT_TWO = 2;
  private static final int GRID_LAYOUT_SPAN_COUNT_FOUR = 4;
  public static final String MOVIE_INTENT_BUNDLE_KEY ="movie_from_list";

  @BindView(R.id.toolbar)
  Toolbar movieListToolbar;
  @BindView(R.id.movie_recycler_view)
  RecyclerView movieListRecyclerView;
  @BindView(R.id.pb_movie_list_loading_bar)
  ProgressBar loadingBar;
  @BindView(R.id.internet_error_layout)
  ConstraintLayout internetErrorLayout;
  @BindView(R.id.tv_error_message)
  TextView errorTextView;
  @BindView(R.id.internet_retry_button)
  Button retryButton;
  @BindView(R.id.error_imageView)
  ImageView errorImageView;
  @Inject
  MovieViewModelFactory factory;

  private MovieListAdapter movieListAdapter;
  private MovieListViewModel moviesViewModel;
  private boolean twoPane;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ButterKnife.bind(this);
    setUpRecyclerView();
    setUpToolbar();
    moviesViewModel = ViewModelProviders.of(this, factory).get(MovieListViewModel.class);

    if (savedInstanceState == null) {
      moviesViewModel.setMoviesViewState(POPULAR);
    }

    if (findViewById(R.id.linear_layout) != null) {
      twoPane = true;
    }

    subscribeObservers();
    retryButton.setOnClickListener(view -> {
      MoviesViewState viewState = moviesViewModel.getMoviesViewState().getValue();
      if (viewState != null && viewState != FAVORITES) {
        moviesViewModel.invalidateDataSource(viewState);
        moviesViewModel.setMoviesViewState(viewState);
      }
    });
  }

  private void subscribeObservers() {
    moviesViewModel.getMoviesViewState().observe(this,
        moviesViewState -> {
          switch (moviesViewState) {
            case POPULAR:
            case TOP_RATED: {
              loadMoviesByViewState(moviesViewState);
              break;
            }
            case FAVORITES: {
              if (twoPane) {
                loadFirstFragmentsInTwoPlane(moviesViewState);
              }
              loadFavoriteMovies();
              break;
            }
          }
        });
  }

  private void loadMoviesByViewState(MoviesViewState moviesViewState) {

    if (twoPane) {
      loadFirstFragmentsInTwoPlane(moviesViewState);
    }

    moviesViewModel.getMovies(moviesViewState)
        .observe(this, movieListAdapter::submitList);
    moviesViewModel.getMoviesStatus(moviesViewState).observe(this, networkStatus -> {
      switch (networkStatus) {
        case SUCCESS: {
          showMovies();
          break;
        }
        case LOADING: {
          showLoadingBar();
          break;
        }
        case ERROR: {
          showErrorMessage(getString(R.string.network_error_message), false);
          break;
        }
        case EMPTY: {
          showErrorMessage(getString(R.string.empty_error_message), false);
          break;
        }
      }
    });
  }

  private void setUpToolbar() {
    setSupportActionBar(movieListToolbar);
    movieListToolbar.setOverflowIcon(getDrawable(R.drawable.ic_sort_24dp));
  }

  private float getScreenWidth() {
    DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
    return displayMetrics.widthPixels / displayMetrics.density;
  }

  private void setUpRecyclerView() {
    GridLayoutManager gridLayoutManager = new GridLayoutManager(this, GRID_LAYOUT_SPAN_COUNT_TWO);

    if (getScreenWidth() > SCREEN_WIDTH_700_DP) {
      gridLayoutManager = new GridLayoutManager(this, GRID_LAYOUT_SPAN_COUNT_FOUR);
    }

    movieListRecyclerView.setLayoutManager(gridLayoutManager);
    movieListRecyclerView.setHasFixedSize(true);
    movieListAdapter = new MovieListAdapter(this);
    movieListRecyclerView.setAdapter(movieListAdapter);
  }

  private void loadFirstFragmentsInTwoPlane(MoviesViewState moviesViewState) {

    if (isNetworkAvailable()) {
      showLoadingInDetailFragment();
      moviesViewModel.getMovies(moviesViewState).observe(this, movies -> {

        if (moviesViewState != FAVORITES) {
          if (!movies.isEmpty()) {
            loadMovieDetailFragment(movies.get(INDEX_ZERO));
            showDetailFragment();
          }
        } else {
          if (!movies.isEmpty()) {
            loadMovieDetailFragment(movies.get(INDEX_ZERO));
            showDetailFragment();
          } else {
            showFavMoviesErrorMessage(getString(R.string.add_favorite_movies_message));
          }
        }
      });
    } else {
      showFavMoviesErrorMessage(getString(R.string.network_error_message));
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_movies_sort, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_popular_movies: {
        moviesViewModel.invalidateDataSource(POPULAR);
        moviesViewModel.setMoviesViewState(POPULAR);
        return true;
      }
      case R.id.action_top_rated_movies: {
        moviesViewModel.invalidateDataSource(TOP_RATED);
        moviesViewModel.setMoviesViewState(TOP_RATED);
        return true;
      }
      case R.id.action_favourite_movies: {
        moviesViewModel.setMoviesViewState(FAVORITES);
        return true;
      }
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void loadFavoriteMovies() {
    LiveData<PagedList<Movie>> favoriteMoviePagedList = moviesViewModel
        .getMovies(FAVORITES);
    showLoadingBar();

    favoriteMoviePagedList.observe(this,
        new Observer<PagedList<Movie>>() {
          @Override
          public void onChanged(PagedList<Movie> movies) {

            if (!movies.isEmpty()) {
              movieListAdapter.submitList(movies);
              showMovies();
            } else {
              showErrorMessage(getString(R.string.add_favorite_movies_message), true);
            }
            favoriteMoviePagedList.removeObserver(this);
          }
        });
  }

  @Override
  public void onResume() {
    super.onResume();
    MoviesViewState moviesViewState = moviesViewModel.getMoviesViewState().getValue();

    if (moviesViewState == FAVORITES) {
      loadFavoriteMovies();
    }
  }

  private boolean isNetworkAvailable() {
    ConnectivityManager manager =
        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = manager.getActiveNetworkInfo();
    boolean isAvailable = false;

    if (networkInfo != null && networkInfo.isConnected()) {
      isAvailable = true;
    }
    return isAvailable;
  }

  private void showMovies() {
    movieListRecyclerView.setVisibility(View.VISIBLE);
    internetErrorLayout.setVisibility(View.GONE);
    loadingBar.setVisibility(View.GONE);
  }

  private void showLoadingBar() {
    movieListRecyclerView.setVisibility(View.GONE);
    internetErrorLayout.setVisibility(View.GONE);
    loadingBar.setVisibility(View.VISIBLE);
  }

  private void showErrorMessage(String message, boolean isFavoriteMovies) {
    movieListRecyclerView.setVisibility(View.GONE);
    errorTextView.setText(message);
    internetErrorLayout.setVisibility(View.VISIBLE);
    retryButton.setVisibility((isFavoriteMovies) ? View.GONE : View.VISIBLE);
    errorImageView.setImageResource(
        (isFavoriteMovies) ? R.drawable.ic_empty_movie_list : R.drawable.ic_internet_error);
    loadingBar.setVisibility(View.GONE);
  }

  private void showDetailFragment() {
    findViewById(R.id.movie_detail_container).setVisibility(View.VISIBLE);
    findViewById(R.id.tv_favorite_movies_error).setVisibility(View.GONE);
    findViewById(R.id.movie_detail_loading_bar).setVisibility(View.GONE);
  }

  private void showLoadingInDetailFragment() {
    findViewById(R.id.movie_detail_container).setVisibility(View.GONE);
    findViewById(R.id.tv_favorite_movies_error).setVisibility(View.GONE);
    findViewById(R.id.movie_detail_loading_bar).setVisibility(View.VISIBLE);
  }

  private void showFavMoviesErrorMessage(String message) {
    findViewById(R.id.movie_detail_container).setVisibility(View.GONE);
    TextView textView = findViewById(R.id.tv_favorite_movies_error);
    textView.setText(message);
    textView.setVisibility(View.VISIBLE);
    findViewById(R.id.movie_detail_loading_bar).setVisibility(View.GONE);
  }

  private void loadMovieDetailFragment(Movie movie) {
    getSupportFragmentManager().beginTransaction()
        .replace(R.id.movie_detail_container, MovieDetailFragment.getInstance(movie))
        .commit();
  }

  @Override
  public void onMovieClick(Movie movie) {
    if (twoPane) {
      showDetailFragment();
      loadMovieDetailFragment(movie);
    } else {
      Intent intentToDetailActivity = new Intent(this, DetailActivity.class);
      intentToDetailActivity.putExtra(MOVIE_INTENT_BUNDLE_KEY, movie);
      startActivity(intentToDetailActivity);
    }
  }
}

