package com.nirmal.jeffrey.popularmovies.userinterface.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ShareCompat.IntentBuilder;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.nirmal.jeffrey.popularmovies.R;
import com.nirmal.jeffrey.popularmovies.adapters.ReviewAdapter;
import com.nirmal.jeffrey.popularmovies.adapters.TrailerAdapter;
import com.nirmal.jeffrey.popularmovies.adapters.TrailerAdapter.OnTrailerClickListener;
import com.nirmal.jeffrey.popularmovies.models.Movie;
import com.nirmal.jeffrey.popularmovies.models.Trailer;
import com.nirmal.jeffrey.popularmovies.userinterface.activities.DetailActivity;
import com.nirmal.jeffrey.popularmovies.viewmodels.MovieDetailViewModel;
import com.nirmal.jeffrey.popularmovies.viewmodels.MovieViewModelFactory;
import com.squareup.picasso.Picasso;
import dagger.android.support.DaggerFragment;
import java.util.ArrayList;
import java.util.Objects;
import javax.inject.Inject;

public class MovieDetailFragment extends DaggerFragment implements OnTrailerClickListener {

  private static final String DATA_BUNDLE_KEY = "movie_detail_fragment";
  private static final String MOVIE_YOUTUBE_WEB_TRAILER_URL = "https://www.youtube.com/watch?v=";
  private static final String MOVIE_YOUTUBE_APP_TRAILER_URL = "vnd.youtube:";
  private static final String BACKDROP_BASE_URL = "http://image.tmdb.org/t/p/w500/";
  private static final String REGEX = "-";
  private static final int INDEX_ZERO = 0;

  @BindView(R.id.movie_detail_trailer_label)
  TextView trailerLabel;
  @BindView(R.id.movie_detail_review_label)
  TextView reviewLabel;
  @BindView(R.id.movie_detail_app_bar)
  AppBarLayout detailAppBarLayout;
  @BindView(R.id.movie_detail_collapsing_tool_bar)
  CollapsingToolbarLayout collapsingToolbarLayout;
  @BindView(R.id.iv_movie_detail_backdrop)
  ImageView backdropImageView;
  @BindView(R.id.movie_detail_tool_bar)
  Toolbar movieDetailToolbar;
  @BindView(R.id.tv_movie_detail_synopsis)
  TextView synopsisTextView;
  @BindView(R.id.rv_movie_detail_review)
  RecyclerView reviewRecyclerView;
  @BindView(R.id.rv_movie_detail_trailer)
  RecyclerView trailerRecyclerView;
  @BindView(R.id.tv_movie_detail_release_date)
  TextView releaseDateTextView;
  @BindView(R.id.tv_movie_detail_title)
  TextView titleTextView;
  @BindView(R.id.fab_movie_detail_favorite)
  FloatingActionButton favoriteFab;
  @BindView(R.id.movie_detail_rating_bar)
  RatingBar ratingBar;
  @BindView(R.id.share_button)
  ImageView shareButton;
  @BindView(R.id.detail_fav_button)
  ImageView favoriteButton;
  @Inject
  MovieViewModelFactory factory;

  private Movie movie;
  private MovieDetailViewModel movieDetailViewModel;
  private boolean isFavoriteButtonClicked;
  private Activity activity;

  public static MovieDetailFragment getInstance(Movie movie) {
    MovieDetailFragment fragment = new MovieDetailFragment();
    Bundle bundle = new Bundle();
    bundle.putParcelable(DATA_BUNDLE_KEY, movie);
    fragment.setArguments(bundle);
    return fragment;
  }

  private static String buildBackDropURLString(String imagePath) {
    return BACKDROP_BASE_URL + imagePath;
  }

  private static Uri buildYoutubeAppVideoUrl(String key) {
    return Uri.parse(MOVIE_YOUTUBE_APP_TRAILER_URL + key);
  }

  private static Uri buildYoutubeWebVideoUrl(String key) {
    return Uri.parse(MOVIE_YOUTUBE_WEB_TRAILER_URL + key);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    activity = (Activity) context;
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.fragment_movie_detail, container, false);
    ButterKnife.bind(this, view);

    if (getArguments() != null) {
      movie = getArguments().getParcelable(DATA_BUNDLE_KEY);
    }

    if (movie != null) {
      setUpToolbar();
      setUpWidgets(movie);
      movieDetailViewModel = ViewModelProviders.of(this, factory).get(MovieDetailViewModel.class);

      if (savedInstanceState == null) {
        subscribeObservers(movie.getId());
      }
      setUpRecyclerViews();

      if (activity instanceof DetailActivity) {
        setUpFavoriteButton(favoriteFab, R.id.fab_movie_detail_favorite,
            R.drawable.ic_favorite_24dp, R.drawable.ic_favorite_border_24dp);
      } else {
        setUpFavoriteButton(favoriteButton, R.id.detail_fav_button,
            R.drawable.ic_favorite_red_24dp, R.drawable.ic_favorite_border_red_24dp);
        favoriteButton.setVisibility(View.VISIBLE);
      }

      initAppBarScrollAnimations();
    }

    return view;
  }

  @Override
  public void onDetach() {
    super.onDetach();
    activity = null;
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.movie_detail, menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (item.getItemId() == R.id.action_share) {
      shareMovie();
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private void shareMovie() {
    Intent intent = IntentBuilder.from(activity)
        .setType(getString(R.string.action_share_type))
        .setChooserTitle(getString(R.string.action_share_title))
        .setText(getString(R.string.share_movie_message, movie.getTitle(), movie.getOverview()))
        .getIntent();

    if (intent.resolveActivity(activity.getPackageManager()) != null) {
      startActivity(intent);
    }
  }

  private void setUpWidgets(Movie movie) {
    synopsisTextView.setText(movie.getOverview());
    titleTextView.setText(movie.getTitle());
    String backdropUrl = buildBackDropURLString(movie.getBackdropPath());
    Picasso.get().load(backdropUrl).into(backdropImageView);
    releaseDateTextView.setText(movie.getReleaseDate().split(REGEX)[INDEX_ZERO]);
    float average = movie.getVoteAverage() / 2;
    ratingBar.setRating(average);
  }

  private void initAppBarScrollAnimations() {
    detailAppBarLayout
        .addOnOffsetChangedListener((AppBarLayout appBarLayout, int verticalOffset) -> {

          if ((Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange()) == 0) {
            favoriteFab.hide();
          } else {
            favoriteFab.show();
          }
        });
  }

  private void subscribeObservers(int movieId) {
    movieDetailViewModel.subscribeToReviewList(movieId);
    movieDetailViewModel.subscribeToTrailerList(movieId);
  }

  private void setUpRecyclerViews() {
    trailerRecyclerView.setHasFixedSize(true);
    trailerRecyclerView
        .setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
    TrailerAdapter trailerAdapter = new TrailerAdapter(this);
    trailerRecyclerView.setAdapter(trailerAdapter);

    movieDetailViewModel.getTrailerList().observe(this,
        trailers -> {

          if (trailers != null && !trailers.isEmpty()) {
            trailerAdapter.setTrailerListData(new ArrayList<>(trailers));
            showTrailers();
          } else {
            hideTrailers();
          }
        });

    reviewRecyclerView.setHasFixedSize(true);
    reviewRecyclerView
        .setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
    ReviewAdapter reviewAdapter = new ReviewAdapter();
    reviewRecyclerView.setAdapter(reviewAdapter);

    movieDetailViewModel.getReviewList().observe(this,
        reviews -> {

          if (reviews != null && !reviews.isEmpty()) {
            reviewAdapter.setReviewListData(new ArrayList<>(reviews));
            showReviews();
          } else {
            hideReviews();
          }
        });
  }

  private void setUpToolbar() {
    if (activity instanceof DetailActivity) {
      AppCompatActivity appCompatActivity = (AppCompatActivity) activity;
      appCompatActivity.setSupportActionBar(movieDetailToolbar);

      if (appCompatActivity.getSupportActionBar() != null) {
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCompatActivity.getSupportActionBar()
            .setHomeAsUpIndicator(R.drawable.ic_arrow_white_24dp);
        setHasOptionsMenu(true);
        collapsingToolbarLayout.setTitle(movie.getTitle());
        collapsingToolbarLayout
            .setCollapsedTitleTextColor(ContextCompat.getColor(activity, android.R.color.white));
        collapsingToolbarLayout
            .setExpandedTitleColor(ContextCompat.getColor(activity, android.R.color.transparent));
      }
    } else {
      favoriteFab.hide();
      detailAppBarLayout.setVisibility(View.GONE);
      shareButton.setOnClickListener(view -> shareMovie());
      shareButton.setVisibility(View.VISIBLE);
    }
  }

  private void setUpFavoriteButton(ImageView fab, int id, int addImageId, int removeImageId) {
    movieDetailViewModel.getMovieFromDB(movie.getId()).observe(this, movieItem -> {

      if (movieItem == null) {
        isFavoriteButtonClicked = false;
        fab.setImageResource(removeImageId);
      } else if (movieItem.getId() == movie.getId()) {
        fab.setImageResource(addImageId);
        isFavoriteButtonClicked = true;
      }
    });

    fab.setOnClickListener(view -> {

      if (isFavoriteButtonClicked) {
        movieDetailViewModel.deleteMovieFromDB(movie.getId());
        fab.setImageResource(removeImageId);
        Snackbar.make(view.findViewById(id),
            getString(R.string.detail_snackbar_deleted_database), Snackbar.LENGTH_SHORT).show();
        isFavoriteButtonClicked = false;
      } else {
        movieDetailViewModel.insertMovieIntoDB(movie);
        fab.setImageResource(addImageId);
        Snackbar.make(view.findViewById(id),
            getString(R.string.detail_snackbar_insert_database), Snackbar.LENGTH_SHORT).show();
        isFavoriteButtonClicked = true;
      }
    });
  }

  @Override
  public void onTrailerClick(Trailer trailer) {
    Uri youtubeAppUri = buildYoutubeAppVideoUrl(trailer.getKey());
    Intent youtubeAppIntent = new Intent(Intent.ACTION_VIEW, youtubeAppUri);
    Uri youtubeWebUri = buildYoutubeWebVideoUrl(trailer.getKey());
    Intent youtubeWebIntent = new Intent(Intent.ACTION_VIEW, youtubeWebUri);

    if (youtubeAppIntent.resolveActivity(Objects.requireNonNull(getContext()).getPackageManager())
        != null) {
      startActivity(youtubeAppIntent);
    } else {
      startActivity(youtubeWebIntent);
    }

  }

  private void hideReviews() {
    reviewLabel.setVisibility(View.GONE);
    reviewRecyclerView.setVisibility(View.GONE);
  }

  private void showReviews() {
    reviewLabel.setVisibility(View.VISIBLE);
    reviewRecyclerView.setVisibility(View.VISIBLE);
  }

  private void showTrailers() {
    trailerLabel.setVisibility(View.VISIBLE);
    trailerRecyclerView.setVisibility(View.VISIBLE);
  }

  private void hideTrailers() {
    trailerLabel.setVisibility(View.GONE);
    trailerRecyclerView.setVisibility(View.GONE);
  }
}
