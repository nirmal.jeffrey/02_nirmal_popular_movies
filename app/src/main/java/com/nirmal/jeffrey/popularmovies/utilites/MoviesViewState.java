package com.nirmal.jeffrey.popularmovies.utilites;

public enum MoviesViewState {
  POPULAR,
  TOP_RATED,
  FAVORITES
}

