package com.nirmal.jeffrey.popularmovies.utilites;

public enum NetworkStatus {
  LOADING,
  SUCCESS,
  EMPTY,
  ERROR
}
