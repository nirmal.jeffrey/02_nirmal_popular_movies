package com.nirmal.jeffrey.popularmovies.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.nirmal.jeffrey.popularmovies.models.Movie;
import com.nirmal.jeffrey.popularmovies.models.Review;
import com.nirmal.jeffrey.popularmovies.models.Trailer;
import com.nirmal.jeffrey.popularmovies.repository.MovieRepository;
import java.util.List;
import javax.inject.Inject;

public class MovieDetailViewModel extends ViewModel {

  private MovieRepository movieRepository;

  @Inject
  public MovieDetailViewModel(MovieRepository movieRepository) {
    this.movieRepository = movieRepository;
  }

  public LiveData<List<Trailer>> getTrailerList() {
    return movieRepository.getTrailerLiveData();
  }

  public LiveData<List<Review>> getReviewList() {
    return movieRepository.getReviewLiveData();
  }

  public void subscribeToTrailerList(int movieId) {
    movieRepository.subscribeToTrailerList(movieId);
  }

  public void subscribeToReviewList(int movieId) {
    movieRepository.subscribeToReviewList(movieId);
  }

  public LiveData<Movie> getMovieFromDB(int movieId) {
    return movieRepository.getMovieFromDB(movieId);
  }

  public void insertMovieIntoDB(Movie movie) {
    movieRepository.insertMovieIntoDB(movie);
  }

  public void deleteMovieFromDB(int movieId) {
    movieRepository.deleteMovieFromDB(movieId);
  }
}
