package com.nirmal.jeffrey.popularmovies.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.PagedList;
import com.nirmal.jeffrey.popularmovies.models.Movie;
import com.nirmal.jeffrey.popularmovies.repository.MovieRepository;
import com.nirmal.jeffrey.popularmovies.utilites.MoviesViewState;
import com.nirmal.jeffrey.popularmovies.utilites.NetworkStatus;
import javax.inject.Inject;

public class MovieListViewModel extends ViewModel {

  private MutableLiveData<MoviesViewState> moviesViewStateLiveData =  new MutableLiveData<>();
  private MovieRepository movieRepository;
  private MediatorLiveData<PagedList<Movie>> popularMovies = new MediatorLiveData<>();
  private MediatorLiveData<PagedList<Movie>> topRatedMovies = new MediatorLiveData<>();

  @Inject
  public MovieListViewModel(MovieRepository repository) {
    this.movieRepository = repository;
    subscribeMovies();
    movieRepository.initStatus();
  }

  public void setMoviesViewState(MoviesViewState moviesViewState) {
    this.moviesViewStateLiveData.setValue(moviesViewState);
  }


  public MutableLiveData<MoviesViewState> getMoviesViewState() {
    return moviesViewStateLiveData;
  }

  private void subscribeMovies() {
    popularMovies
        .addSource(movieRepository.getPagedPopularMoviesLiveData(), popularMovies::setValue);
    topRatedMovies
        .addSource(movieRepository.getPagedTopRatedMoviesLiveData(), topRatedMovies::setValue);
  }

  public LiveData<PagedList<Movie>> getMovies(MoviesViewState moviesViewState) {

    switch (moviesViewState) {
      case POPULAR: {
        return popularMovies;
      }
      case TOP_RATED: {
        return topRatedMovies;
      }
      case FAVORITES: {
        return movieRepository.getPagedFavoriteMoviesListData();
      }
      default:
        throw new IllegalArgumentException("Invalid movie view state");
    }
  }

  public void invalidateDataSource(MoviesViewState moviesViewState) {
    switch (moviesViewState){
      case POPULAR: {
        movieRepository.invalidatePopularDataSource();
        break;
      }
      case TOP_RATED:{
        movieRepository.invalidateTopRatedDataSource();
        break;
      }
      default:
        throw new IllegalArgumentException("Invalid movie view state");
    }
  }

  public LiveData<NetworkStatus> getMoviesStatus(MoviesViewState moviesViewState) {
    switch (moviesViewState) {
      case TOP_RATED: {
        return movieRepository.getTopRatedMoviesStatus();
      }
      case POPULAR: {
        return movieRepository.getPopularMoviesStatus();
      }
      default:
        throw new IllegalArgumentException("Invalid movie view state");
    }
  }
}
