package com.nirmal.jeffrey.popularmovies;

import com.nirmal.jeffrey.popularmovies.models.Movie;

public class TestUtils {

  private static final String TITLE = "Godzilla: King of the Monsters";
  private static final String BACKDROP_PATH = "/cNt14e43I2DDW6Xd9zFhrP8eOcA.jpg";
  private static final int ID = 373571;
  private static final String POSTER_PATH = "/fQ40gmFM4p03tXwMxQQKh2cCBW4.jpg";
  private static final int VOTE_COUNT = 1477;
  private static final double POPULARITY = 83.487;
  private static final float VOTE_AVERAGE = 6.2f;
  private static final String OVERVIEW = "Follows the heroic efforts of the crypto-zoological agency";
  private static final String RELEASE_DATE = "2019-05-31";

  public static final Movie TEST_MOVIE_ONE = new Movie(POPULARITY, VOTE_COUNT, POSTER_PATH,
      ID, BACKDROP_PATH, TITLE, VOTE_AVERAGE, OVERVIEW, RELEASE_DATE);
}
