package com.nirmal.jeffrey.popularmovies.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import com.nirmal.jeffrey.popularmovies.TestUtils;
import org.junit.Test;

public class MovieTest {

  private static final String TITLE = "King Kong";

  @Test
  public void isMoviesEqualsIdenticalPropertiesReturnTrue() throws Exception {
    Movie movieOne = new Movie(TestUtils.TEST_MOVIE_ONE);
    Movie movieTwo = new Movie(TestUtils.TEST_MOVIE_ONE);
    assertEquals(movieOne, movieTwo);
  }

  @Test
  public void isMoviesEqualsDifferentPropertiesReturnTrue() throws Exception {
    Movie movieOne = new Movie(TestUtils.TEST_MOVIE_ONE);
    movieOne.setTitle(TITLE);
    Movie movieTwo = new Movie(TestUtils.TEST_MOVIE_ONE);
    assertNotEquals(movieOne, movieTwo);
  }
}
